Stride API Tour of Actions
=================

This Project demonstrates the many ways Stride API Actions work, can be triggered, and the information provided to the server and dialogs.

The Stride - Tour of Actions app demonstrates many features of the Stride API. 
It has [Glances](https://developer.atlassian.com/cloud/stride/learning/glances-sidebars/),
[Sidebars](https://developer.atlassian.com/cloud/stride/learning/glances-sidebars/),
[Configuration dialogs](https://developer.atlassian.com/cloud/stride/learning/config-pages/),
[Dialogs](https://developer.atlassian.com/cloud/stride/learning/dialogs/),
[conversation creation](https://developer.atlassian.com/cloud/stride/rest/#api-site-cloudId-conversation-post),
and the many [Action types](https://developer.atlassian.com/cloud/stride/learning/actions/) supported in [messages, dialogs, message actions, input actions, and sidebars.](https://developer.atlassian.com/cloud/stride/learning/adding-actions/)     

Follow instructions on this page to create your own Stride - Tour of Actions app.

## Play with the App
You can quickly get started by installing this application in your Stride instance. If you want to modify the code and see log messages follow the instructions from **Remix this project** onward.  

<div class='InstallButton'></div>


## Remix this project

<a href="https://glitch.com/edit/#!/remix/stride-tour-of-actions" dynamic-url="https://glitch.com/edit/#!/remix/{{glitch_project_name}}"><img src="https://cdn.glitch.com/2703baf2-b643-4da7-ab91-7ee2a2d00b5b%2Fremix-button.svg" alt="Remix on Glitch" /></a>

<a href="https://glitch.com/edit/#!/stride-tour-of-actions" dynamic-url="https://glitch.com/edit/#!/{{glitch_project_name}}">View Code on Glitch</a>
<a href="https://bitbucket.org/atlassian/stride-tour-of-actions-demo/src/master/">View Code on Bitbucket</a>

## Create an app

To create a Stride app:

1. Open the <a href="https://developer.atlassian.com/apps/create" target="_blank">Create an App</a> page.
1. Give your new app a name in the **App name** field.
1. If desired, add a short description in the **Description** field.
1. Click **Create**; you'll be directed to your app's dashboard page.
1. Click **Enable API** for the Stride API.
1. Click **Add** for the **Manage conversations** Api Scope
1. Use the Copy button on the **Enabled APIs** tab to copy the **Client Id** and the **Client Secret** for use below as the `{clientId}` and `{clientSecret}`.
1. In your glitch app put your client ID and client secret in the `.env` file. You can safely store app secrets in `.env` (nobody can see this but you and people you invite)

### Link the app descriptor

The [app descriptor](https://developer.atlassian.com/cloud/stride/blocks/app-descriptor) for the 
Stride - Tour of Actions app is hosted at [/descriptor](/descriptor). You have to link it to your app 
in the app configuration page where you created your app earlier.

To link the app descriptor to your app:

1. Navigate to your <a href="https://developer.atlassian.com/apps" target="_blank">My Apps</a> page.
1. Click to open the app and then click the **Install** tab.
1. Enter your app descriptor URL, `https://{{glitch_project_name}}.glitch.me/descriptor`, in the **Descriptor URL** field.
1. Click **Refresh**. When the app descriptor is installed you will see a *The descriptor has been updated successfully!* message displayed.

## Install the app

Now your app is created and configured, and your app descriptor is linked, and you can add the app to a conversation:

<div class="InstallButton"></div>

## Install the app through the user interface

1. In your app dashboard, in the **Install** tab, click **Copy** for the **Installation URL**.
1. Open Stride.
1. Open the conversation in which you'd like to install the app.
1. Click ![apps_icon](https://developer.atlassian.com/cloud/stride/images/apps_icon.png) to open the **Apps** sidebar, and then click the **+** button to open the Atlassian Marketplace in Stride.
1. Click **Connect your app** in the **Connect your own app** box, and then select the **Installation URL** tab.
1. Paste in the **Installation URL**. Your app information should appear in the window.
1. Click **Agree**.

In a few seconds, your newly-installed app should appear in the **Apps** sidebar, and the app should send a message to the conversation.

## Test the app

Your app is ready to roll, so let's see it in action.

When the App is installed it will post several messages in the conversation it was installed in. 
Click on the actions and see how they work. There are also now Message and Input actions that the application installed.
You can see those by clicking on the ellipsis (the `...`) next to the message box or at the top of any message.  

Any actions that hit the service will post a message to the conversation containing the context and body the call received.    


You can inspect each message using the Stride [Developer Toolkit](https://developer.atlassian.com/cloud/stride/developer-toolkit/).


## Next steps

* To see how the messages are created, look at `/messages/messageWithActions.js`
* Uncomment `<pre><code id="console"></code></pre>` and 
`` $('#console').append(`Event received:  ${callback._context.eventName}\n`); ``
 in the html pages and see what other events show up.
* Inspect the messages using the [Developer Toolkit](https://developer.atlassian.com/cloud/stride/developer-toolkit/)
* Read through our **Learning** section, which has more 
 [tutorials](https://developer.atlassian.com/cloud/stride/tutorials) and explains different concepts including 
 [Glances and Sidebars](https://developer.atlassian.com/cloud/stride/learning/glances-sidebars/),
 [conversations](https://developer.atlassian.com/cloud/stride/learning/conversations), 
 [Configuration dialogs](https://developer.atlassian.com/cloud/stride/learning/config-pages/),
 [Dialogs](https://developer.atlassian.com/cloud/stride/learning/dialogs/),
 [bots](/cloud/stride/learning/bots), and more.
* Take a look at [Hello World for Stride](https://developer.atlassian.com/cloud/stride/getting-started/),
 or our fully featured [Reference App](https://developer.atlassian.com/cloud/stride/reference-app). The reference app 
 has quite a bit more functionality built in, and is really a showcase of what you can do with the API in Stride.